CC=/usr/bin/gcc
CFLAGS= -g -Wall -I.
LINKFLAGS= -g -Wall

all: raid

raid: raid5.o utils.o
	$(CC) $(LINKFLAGS) -o raid raid5.o utils.o

raid5.o: raid5.c utils.h
	$(CC) $(CFLAGS) -c raid5.c -o raid5.o

clean:
	rm -f *.o chat test
