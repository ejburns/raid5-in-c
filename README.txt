Step 1:
	Run the hw3_setup bash script. You can change the variables at the top to match what you want the loop device paths to be.

Step 2:
	Put some files on the drives, in folders mounts/f0 and mounts/f1.
	
Step 3:
	Unmount the drives.

Step 4:
	Make the program.
	
	make
	
Step 5:
	Run the program, which will create the parity, automate the formatting of /dev/loop1, recover the image, and then remount the drives.
	
	./raid /dev/loop0 /dev/loop1 /dev/loop2
	
	You may replace loop0, loop1, or loop2 with the paths your own devices. Paths must be provided for the program to work properly.
	The structure of the program is such that loop0 and loop1 are mapped to the storage images, where loop2 is mapped to the parity.
	
Step 6:
	Open up the folders and see that the contents are still there.
