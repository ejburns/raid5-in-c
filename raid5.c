#include <stdio.h>    // for io
#include <stddef.h>   // for size_t
#include <unistd.h>   // for write/read
#include <stdlib.h>   // for malloc
#include <fcntl.h>    // for O_CREAT, etc.
#include <string.h>   // for strlen
#include <errno.h>
#include "utils.h"

#define DEBUG 1

char *getBlockBuffer(int fd, int block_size);
void createParity(const char* loop0, const char* loop1, const char* loop2, int block_size);
void recover(const char* loop0, const char* loop1, const char* loop2, int block_size);

//
// MAIN
//
int main(int argc, char** argv) {
	
	// Get the command line parameters
	if (argc != 4) {
		puts("Must provide the paths to the 3 loops devices.");
		return 1;
	}

	// Set the passed parameters to variables
	const char* loop0 = argv[1];
	const char* loop1 = argv[2];
	const char* loop2 = argv[3];

	// Get the block sizes of the images
	int block_size = getBlockSize(loop0);
	
	// Create the parity
	createParity(loop0, loop1, loop2, block_size);
	
	// Format loop1
	system("sudo mkfs.vfat /dev/loop1");
	if (DEBUG) puts("Formatted loop1.");
	
	// Recover the drive
	recover(loop0, loop1, loop2, block_size);
	
	// Remount the loop devices
	system("sudo mount /dev/loop0 -t vfat -o rw,sync mounts/f0");
	system("sudo mount /dev/loop1 -t vfat -o rw,sync mounts/f1");
	if (DEBUG) puts("Remounted the loop devices.");
	
	return 0;
}

//
// FUNCTIONS
//

/* gets a block as a buffer: */
char *getBlockBuffer(int fd, int block_size) {
	char *img = (char*) malloc(block_size);
	read(fd, img, block_size);
	return img;
}

/* creates the parity of the file */
void createParity(const char* loop0, const char* loop1, const char* loop2, int block_size) {
	// Open up the block devices
	int fd0 = open(loop0, O_RDWR, 0666);
	int fd1 = open(loop1, O_RDWR, 0666);
	int fd2 = open(loop2, O_RDWR, 0666);
	if (DEBUG) puts("Opened up the files.");

	// Get the blocks as a buffer
	char *img0 = getBlockBuffer(fd0, block_size);
	char *img1 = getBlockBuffer(fd1, block_size);
	
	// Do a parity write
	parityWrite(fd0,fd1,fd2,img0,img1,block_size);
	if (DEBUG) puts("Did the parity write.");

	// Close the files
	close(fd0);
	close(fd1);
	close(fd2);
	if (DEBUG) puts("Closed the files.");
	
	// Free the buffers
	free(img0);
	free(img1);
	if (DEBUG) puts("Freed the buffers.");
}

/* recovers the disk with parity: */
void recover(const char* loop0, const char* loop1, const char* loop2, int block_size) {
	// Open loop0 and loop2
	int fd0 = open(loop0, O_RDWR, 0666);
	int fd1 = open(loop1, O_RDWR, 0666);
	int fd2 = open(loop2, O_RDWR, 0666);
	if (DEBUG) puts("Opened loop0, loop1, and loop2");

	// Create a buffer to store the recovered image
	char *img1_rec = (char*)malloc(block_size);
	if (DEBUG) puts("Got a buffer for reading the parity.");

	// Do a parity read
	parityRead(fd0, fd2, img1_rec, block_size);
	if (DEBUG) puts("Did the parity read.");

	// Write the contents of the buffer back to the image
	int i;
	for (i = 0; i < block_size; i++) {
		write(fd1, &img1_rec[i], sizeof(char));
	}
	
	// Close the files
	close(fd0);
	close(fd1);
	close(fd2);
	if (DEBUG) puts("Closed the files.");

	// Free the buffer
	free(img1_rec);
	if (DEBUG) puts("Freed the recovery buffer.");
}
