#include <fcntl.h>
#include <stdio.h>    // for io
#include <unistd.h>   // for write/read
#include <linux/fs.h>
#include <sys/ioctl.h> // for ioctl

int main(int argc, char **argv)
{
  int fd;
  unsigned long numblocks=0;

  fd = open(argv[1], O_RDONLY);
  ioctl(fd, BLKGETSIZE, &numblocks);
  close(fd);
  printf("Number of blocks: %lu, this makes %.3f GB\n",
	 numblocks, 
	 (double)numblocks * 512.0 / (1024 * 1024 * 1024));
	 
	 return 0;
}
