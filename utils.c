#include <sys/ioctl.h> // for ioctl
#include <linux/fs.h>
#include <unistd.h>   // for write/read
#include <fcntl.h>    // for O_CREAT, etc.
#include <stddef.h>   // for size_t
#include <stdio.h>    // for io
#include <stdlib.h>   // for malloc
#include "utils.h"

/* getBlockSize: */
int getBlockSize(const char *path)
{
	int fd;
	unsigned long numblocks=0;

	fd = open(path, O_RDONLY);
	ioctl(fd, BLKGETSIZE, &numblocks);
	close(fd);
	return numblocks * 512;
}

/* parityWrite: */
size_t parityWrite(
	int fd0, int fd1, int fd2,
	const void *buf0, const void *buf1,
	size_t count) {
		
	size_t i;
	for(i = 0; i < count; i++) {
			char byte0 = ((char*)buf0)[i];
			char byte1 = ((char*)buf1)[i];
			char parity = byte0 ^ byte1;
			write(fd0, &byte0, sizeof(char));
			write(fd1, &byte1, sizeof(char));
			write(fd2, &parity, sizeof(char));
	}
	return count;
}

/* parityRead: */
size_t parityRead(int fd0, int fd1, void *buf, size_t count) {
	char *buff0 = (char*)malloc(count);
	char *buff1 = (char*)malloc(count);
	char *buff = (char*)buf;
	read(fd0, buff0, count);
	read(fd1, buff1, count);
	size_t i;
	for(i = 0; i < count; i++) {
			buff[i] = buff0[i] ^ buff1[i];
	}
	return count;
}
